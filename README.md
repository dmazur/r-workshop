# README #

These files are for the exercises in the McGill HPC Statistical Computing with R workshop. 

Please see the workshop slides for exercise instructions:
http://www.hpc.mcgill.ca/index.php/training#intro_r

Questions may be directed to guillimin@calculquebec.ca
